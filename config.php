<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin SignUp for Pluxml
 *
 * @package  SignUp
 * @version  1.0.0
 * @date  29.07.2020
 * @author   Thomas Ingles, sudwebdesign.free.fr
 **/
# Controle du token du formulaire
plxToken::validateFormToken($_POST);

if(!empty($_POST)) {
	$plxPlugin->setParam('mnuDisplay', $_POST['mnuDisplay'], 'numeric');
	$plxPlugin->setParam('mnuName', $_POST['mnuName'], 'string');
	$plxPlugin->setParam('mnuPos', $_POST['mnuPos'], 'numeric');
	$plxPlugin->setParam('label', $_POST['label'], 'numeric');
	$plxPlugin->setParam('all', $_POST['all'], 'numeric');
	$plxPlugin->setParam('bot', $_POST['bot'], 'numeric');
	$plxPlugin->saveParams();
	header('Location: ');
	exit;
}
$mnuDisplay = intval($plxPlugin->getParam('mnuDisplay'));
$mnuName = empty($plxPlugin->getParam('mnuName')) ? $plxPlugin->getLang('L_MENU_NAME') : $plxPlugin->getParam('mnuName');
$mnuPos = $plxPlugin->getParam('mnuPos')=='' ? 2 : $plxPlugin->getParam('mnuPos');
$label = intval($plxPlugin->getParam('label'));
$all = intval($plxPlugin->getParam('all'));#all the time
$bot = intval($plxPlugin->getParam('bot'));#(ro)bot
?>
<h2 class="hide"><?php echo $plxPlugin->getInfo('title') ?></h2>
<p class="in-action-bar"><?php echo $plxPlugin->lang('L_SUB_TITLE') ?></p>
<form id="form" action="" method="post">
 <?php echo plxToken::getTokenPostMethod() ?>
 <fieldset>
  <p><input type="submit" name="submit" value="<?php $plxPlugin->lang('L_SAVE') ?>" /></p>
  <p class="field"><label for="id_mnuDisplay"><?php echo $plxPlugin->lang('L_MENU_DISPLAY') ?>&nbsp;:</label></p>
  <?php plxUtils::printSelect('mnuDisplay',array(L_NO,L_YES),$mnuDisplay); ?>
  <p class="field"><label for="id_mnuName"><?php $plxPlugin->lang('L_MENU_TITLE') ?>&nbsp;:</label></p>
  <?php plxUtils::printInput('mnuName',$mnuName,'text','20-30') ?>
  <p class="field"><label for="id_mnuPos"><?php $plxPlugin->lang('L_MENU_POS') ?>&nbsp;:</label></p>
  <?php plxUtils::printInput('mnuPos',$mnuPos,'text','2-5') ?>
  <p class="field"><label for="id_label"><?php $plxPlugin->lang('L_LABEL') ?>&nbsp;:</label></p>
  <?php plxUtils::printSelect('label',array(L_NO,L_YES),$label) ?>
  <p class="field"><label for="id_all"><?php $plxPlugin->lang('L_ALL') ?>&nbsp;:</label></p>
  <?php plxUtils::printSelect('all',array(L_NO,L_YES),$all) ?>
  <p class="field"><label for="id_bot"><?php $plxPlugin->lang('L_BOT') ?>&nbsp;:</label></p>
  <?php plxUtils::printSelect('bot',array(L_NO,L_YES),$bot) ?>

 </fieldset>
</form>
