<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin SignUp for Pluxml
 *
 * @package  SignUp
 * @version  1.0.0
 * @date  29.07.2020
 * @author   Thomas Ingles, sudwebdesign.free.fr
 **/
class SignUp extends plxPlugin {
 public function __construct($default_lang){
   # Appel du constructeur de la classe plxPlugin (obligatoire)
   parent::__construct($default_lang);
   # droits pour accèder à la page config.php du plugin
   $this->setConfigProfil(PROFIL_ADMIN);
   if(defined('PLX_ADMIN')) {# Déclarations des hooks pour la zone d'administration
     $this->addHook('AdminAuthPrepend', 'AdminAuthPrepend');#save new on post
     $this->addHook('AdminAuthEndHead', 'AdminAuthEndHead');#inc css form.
     if(!$this->getParam('all')){#no showall
       if(defined('PLX_VERSION') AND version_compare(PLX_VERSION, '5.7', '>')) {#>5.7
         $this->addHook('AdminAuthBegin', 'AdminAuthBegin');#get != 'new=1'
       }else{#>=5.8
         $this->addHook('AdminAuthTop', 'AdminAuthBegin');#get != 'new=1'
       }
     }
     $this->addHook('AdminAuthEndBody', 'AdminAuthEndBody');#inc form.signup if get == 'new=1'
   }else{
     if($this->getParam('mnuDisplay')) {
       $this->addHook('plxShowStaticListEnd', 'plxShowStaticListEnd');#add menu
     }
   }
 }
 public function AdminAuthEndHead(){#inc css/auth.css need in form.signup
?>
	<link rel="stylesheet" type="text/css" href="<?php echo PLX_PLUGINS . __CLASS__ ?>/css/auth.css" media="screen" />
<?php
 }
 public function AdminAuthPrepend(){#save new on post
echo '<?php #' . __CLASS__ . '::' . __FUNCTION__ . PHP_EOL; ?>
   #New user
   if(!empty($_POST) AND !empty($_POST['update']) AND !empty($_POST['name']) AND !empty($_POST['wall-e']) AND !empty($_POST['password']) AND !empty($_POST['login']) AND empty($_POST['email'])) {
     $posts = array();
     $posts['userNum'] = array();
     $posts['newuser'] = 'true';
     $posts['update'] = $_POST['update'];
     # On récupère le dernier identifiant
     $a = array_keys($plxAdmin->aUsers);
     rsort($a);
     $new_userid = str_pad($a['0']+1, 3, "0", STR_PAD_LEFT);
     # On integre le nouvel utilisateur
     $posts['userNum'][] = $new_userid;
     $posts[$new_userid.'_newuser'] = 'true';
     $posts[$new_userid.'_name'] = $_POST['name'];
     $posts[$new_userid.'_infos'] = '';
     $posts[$new_userid.'_login'] = $_POST['login'];
     $posts[$new_userid.'_password'] = $_POST['password'];
     $posts[$new_userid.'_email'] = $_POST['wall-e'];#Real mail field ;)
     $posts[$new_userid.'_profil'] = PROFIL_WRITER;
     $posts[$new_userid.'_active'] = 1;
     $_SESSION['user'] = '1000';#need in editUsers()
     #On enregistre le nouvel utilisateur
     if(!$plxAdmin->editUsers($posts)) {
       $msgSignUp = $_SESSION['error'];
       unset($_SESSION['error'], $_POST['login'], $_POST['password']);
       $errorSignUp = 'alert red';
     }
     unset($_SESSION['user']);#remove fake
   }
?><?PHP
 }
 public function plxShowStaticListEnd() {#add menu
   echo '<?php '; ?>
   array_splice($menus, '<?=($this->getParam('mnuPos')-1)?>', 0, '<li><a rel="nofollow" class="static" href="'.$this->plxMotor->urlRewrite('core/admin').'/auth.php?new=1" title="<?=plxUtils::strCheck($this->getParam('mnuName'))?>"><?=plxUtils::strCheck($this->getParam('mnuName'))?></a></li>');
?><?php
 }
 public function AdminAuthBegin(){#get != 'new=1' (<5.8 == AdminAuthTop)
echo '<?php '; ?>
  if($plxAdmin->get == 'new=1'){
    ob_start();
  }
?><?php
 }
 public function AdminAuthEndBody(){#get == 'new=1' or all
echo '<?php
$label='.intval($this->getParam('label')).';
$noBots='.intval(!$this->getParam('bot')).';
$showAll='.intval($this->getParam('all')).';
';
?>
  $formSignUp = '';
  if($showAll OR $plxAdmin->get == 'new=1') {# get == new=1
    if(!$showAll) ob_end_clean();
    define('L_BACK_SIGNUP', '<?php $this->lang('L_BACK') ?>');
    define('L_SHIFT_SIGNUP', '<?php $this->lang('L_SHIFT') ?>');
    define('L_PASSWORD_SIGNUP', '<?php $this->lang('L_PASSWORD') ?>');
    if($noBots) ob_start();
    include PLX_PLUGINS . '<?=__CLASS__?>' . DIRECTORY_SEPARATOR . 'form.signup.php';
    if($noBots) $formSignUp = ob_get_clean();
  }else{
    $formSignUp = '
				<p class="text-center">
					<small><a rel="nofollow" href="auth.php?new=1"><?=plxUtils::strCheck($this->getParam('mnuName'))?></a></small>
				</p>' . PHP_EOL;
  }
  if($noBots) {# bots protect by ob + js
    echo '<noscript><p class="text-center"><a  class="text-orange"rel="noreferer" href="https://enable-javascript.com/<?=$this->default_lang?>">⚛ JavaScript?</a></p></noscript><script type="text/javascript">document.write(JSON.parse(JSON.stringify('.json_encode($formSignUp).')));</script>';
  } else {
    echo $formSignUp;
  }
?><?php
 }
}