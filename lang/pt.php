<?php
$LANG = array(
'L_BACK'     => 'Casa',
'L_SHIFT'    => 'Conexão',
'L_PASSWORD' => 'Senha',
# config.php
'L_SUB_TITLE'    => 'Permite que os usuários se registrem',
'L_MENU_DISPLAY' => 'Exibir menu para se inscrever',
'L_MENU_TITLE'   => 'Título do menu',
'L_MENU_NAME'    => 'Registro',
'L_MENU_POS'     => 'Posição do menu',
'L_LABEL'        => 'Ver rótulos',
'L_SAVE'         => 'Salvar',
'L_ALL'          => 'Sempre mostrar forma',
'L_BOT'          => 'Forma visível para robôs',
);