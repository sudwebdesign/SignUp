<?php
$LANG = array(
'L_BACK'     => 'acuèlh',
'L_SHIFT'    => 'noselança',
'L_PASSWORD' => 'diccion de passa',
# config.php
'L_SUB_TITLE'    => 'Permet als internautes de s’enregistres',
'L_MENU_DISPLAY' => 'cartelar al menu grèu s’enregistrer',
'L_MENU_TITLE'   => 'Titre dels menu',
'L_MENU_NAME'    => 's enregistrer',
'L_MENU_POS'     => 'Position dels menu',
'L_LABEL'        => 'cartelar suls labels',
'L_SAVE'         => 'registrar',
'L_ALL'          => 'totjorn cartelar al formulaire',
'L_BOT'          => 'Formulaire visible per suls robots',
);