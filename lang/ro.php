<?php
$LANG = array(
'L_BACK'     => 'Acasă',
'L_SHIFT'    => 'Conexiune',
'L_PASSWORD' => 'Parola',
# config.php
'L_SUB_TITLE'    => 'Permite utilizatorilor să se înregistreze',
'L_MENU_DISPLAY' => 'Vizualizare meniu pentru înregistrare',
'L_MENU_TITLE'   => 'Titlu meniu',
'L_MENU_NAME'    => 'Înregistrează-te',
'L_MENU_POS'     => 'Poziția meniului',
'L_LABEL'        => 'Vizualizarea etichetelor',
'L_SAVE'         => 'Salva',
'L_ALL'          => 'Se afișează întotdeauna formularul',
'L_BOT'          => 'Formular vizibil pentru roboți',
);