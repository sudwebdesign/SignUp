<?php
$LANG = array(
'L_BACK'     => 'Casa',
'L_SHIFT'    => 'Conexión',
'L_PASSWORD' => 'Contraseña',
# config.php
'L_SUB_TITLE'    => 'Permite a los usuarios registrarse',
'L_MENU_DISPLAY' => 'Ver menú para registrarse',
'L_MENU_TITLE'   => 'Título del menú',
'L_MENU_NAME'    => 'Registro',
'L_MENU_POS'     => 'Posición del menú',
'L_LABEL'        => 'Ver etiquetas',
'L_SAVE'         => 'Salvar',
'L_ALL'          => 'Mostrar siempre el formulario',
'L_BOT'          => 'Forma visible para los robots',
);