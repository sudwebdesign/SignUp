<?php
$LANG = array(
'L_BACK'     => 'Accueil',
'L_SHIFT'    => 'Connexion',
'L_PASSWORD' => 'Mot de passe',
# config.php
'L_SUB_TITLE'    => 'Permet aux internautes de s’enregistrés',
'L_MENU_DISPLAY' => 'Afficher le menu pour s’enregistrer',
'L_MENU_TITLE'   => 'Titre du menu',
'L_MENU_NAME'    => 'S’enregistrer',
'L_MENU_POS'     => 'Position du menu',
'L_LABEL'        => 'Afficher les labels',
'L_SAVE'         => 'Enregistrer',
'L_ALL'          => 'Toujours Afficher le formulaire',
'L_BOT'          => 'Formulaire visible par les robots',
);