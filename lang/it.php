<?php
$LANG = array(
'L_BACK'     => 'Casa',
'L_SHIFT'    => 'Connessione',
'L_PASSWORD' => 'Password',
# config.php
'L_SUB_TITLE'    => 'Consente agli utenti di registrarsi',
'L_MENU_DISPLAY' => 'Menu Visualizza per registrarsi',
'L_MENU_TITLE'   => 'Titolo del menu',
'L_MENU_NAME'    => 'Registro',
'L_MENU_POS'     => 'Posizione del menu',
'L_LABEL'        => 'Visualizzare le etichette',
'L_SAVE'         => 'Salvare',
'L_ALL'          => 'Mostra sempre modulo',
'L_BOT'          => 'Forma visibile ai robot',
);