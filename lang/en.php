<?php
$LANG = array(
'L_BACK'     => 'Home',
'L_SHIFT'    => 'Connection',
'L_PASSWORD' => 'Password',
# config.php
'L_SUB_TITLE'    => 'Allows users to register',
'L_MENU_DISPLAY' => 'View menu to register',
'L_MENU_TITLE'   => 'Menu title',
'L_MENU_NAME'    => 'Register',
'L_MENU_POS'     => 'Menu position',
'L_LABEL'        => 'View labels',
'L_SAVE'         => 'Save',
'L_ALL'          => 'Always Show form',
'L_BOT'          => 'Form visible to robots',
);