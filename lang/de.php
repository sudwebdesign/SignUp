<?php
$LANG = array(
'L_BACK'     => 'Startseite',
'L_SHIFT'    => 'Verbindungsanschluss',
'L_PASSWORD' => 'Passwort',
# config.php
'L_SUB_TITLE'    => 'Ermöglicht es Internetnutzern, sich zu registrieren',
'L_MENU_DISPLAY' => 'Menü zum Speichern anzeigen',
'L_MENU_TITLE'   => 'Titel des Menüs',
'L_MENU_NAME'    => 'Einchecken',
'L_MENU_POS'     => 'Menüposition',
'L_LABEL'        => 'Labels anzeigen',
'L_SAVE'         => 'Speichern',
'L_ALL'          => 'Immer das Formular anzeigen',
'L_BOT'          => 'Für Roboter sichtbares Formular',
);