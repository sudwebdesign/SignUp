<?php
$LANG = array(
'L_BACK'     => 'Home',
'L_SHIFT'    => 'Verbinding',
'L_PASSWORD' => 'Wachtwoord',
# config.php
'L_SUB_TITLE'    => 'Hiermee kunnen gebruikers zich registreren',
'L_MENU_DISPLAY' => 'Menu weergeven om te registreren',
'L_MENU_TITLE'   => 'Menutitel',
'L_MENU_NAME'    => 'Registreren',
'L_MENU_POS'     => 'Menupositie',
'L_LABEL'        => 'Labels weergeven',
'L_SAVE'         => 'Opslaan',
'L_ALL'          => 'Formulier altijd weergeven',
'L_BOT'          => 'Vorm zichtbaar voor robots',
);