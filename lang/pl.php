<?php
$LANG = array(
'L_BACK'     => 'W: strona główna',
'L_SHIFT'    => 'Połączenia',
'L_PASSWORD' => 'Hasło',
# config.php
'L_SUB_TITLE'    => 'Umożliwia użytkownikom rejestrację',
'L_MENU_DISPLAY' => 'Wyświetl menu, aby się zarejestrować',
'L_MENU_TITLE'   => 'Tytuł menu',
'L_MENU_NAME'    => 'Zarejestrować',
'L_MENU_POS'     => 'Pozycja menu',
'L_LABEL'        => 'Wyświetlanie etykiet',
'L_SAVE'         => 'Zapisz',
'L_ALL'          => 'Zawsze pokaż formularz',
'L_BOT'          => 'Forma widoczna dla robotów',
);