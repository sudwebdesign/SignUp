# SignUp - [pluXml](https://www.pluxml.org) - Plugin

* DE: Ermöglicht es Internetnutzern, sich zu registrieren
* EN: Allows users to register'
* ES: Permite a los usuarios registrarse
* FR: Permet aux internautes de s’enregistrés
* IT: Consente agli utenti di registrarsi
* NL: Hiermee kunnen gebruikers zich registreren
* OC: Permet als internautes de s’enregistres
* PL: Umożliwia użytkownikom rejestrację
* PT: Permite que os usuários se registrem
* RO: Permite utilizatorilor să se înregistreze
* RU: Позволяет пользователям регистрироваться

#### Notes : Icons of css/auth.css from [utf8icons](https://www.utf8icons.com/) 😍